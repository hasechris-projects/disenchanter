package com.github.impelon.disenchanter.common.core;

import net.minecraft.state.BooleanProperty;

public class DisenchanttableProperties {
    public static final BooleanProperty AUTOMATIC = BooleanProperty.create("automatic");
    public static final BooleanProperty BULKDISENCHANTMENT = BooleanProperty.create("bulkdisenchantment");
    public static final BooleanProperty VOIDING = BooleanProperty.create("voiding");
}
