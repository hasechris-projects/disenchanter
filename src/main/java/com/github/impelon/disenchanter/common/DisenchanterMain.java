package com.github.impelon.disenchanter.common;

import com.github.impelon.disenchanter.client.ClientProxy;
import com.github.impelon.disenchanter.common.core.DisenchanttableReference;
import com.github.impelon.disenchanter.server.ServerProxy;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

// The value here should match an entry in the META-INF/mods.toml file
@Mod(value = DisenchanttableReference.MODID)
public class DisenchanterMain
{
    private static DisenchanterMain instance;
    private static final Logger LOGGER = LogManager.getLogger();
    //public static final String MODID = "disenchanter";
    public static ServerProxy proxy = DistExecutor.runForDist(() -> ClientProxy::new, () -> ServerProxy::new);

    public DisenchanterMain() {

        instance = this;
        MinecraftForge.EVENT_BUS.register(this);
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::preInit);

    }

    private void preInit(final FMLCommonSetupEvent event) {
        proxy.preInit();

        //DistExecutor.runWhenOn(Dist.CLIENT);
    }



}
