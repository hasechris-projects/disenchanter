package com.github.impelon.disenchanter.common.blocks;

import com.github.impelon.disenchanter.common.core.DisenchanttableProperties;
import com.github.impelon.disenchanter.common.tileentity.DisenchanttableTileEntity;
import com.github.impelon.disenchanter.common.util.DefaultNames;
import net.minecraft.block.*;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.ServerPlayerEntity;
import net.minecraft.inventory.InventoryHelper;
import net.minecraft.inventory.container.INamedContainerProvider;
import net.minecraft.state.BooleanProperty;
import net.minecraft.state.StateContainer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.BlockRayTraceResult;
import net.minecraft.util.math.shapes.ISelectionContext;
import net.minecraft.util.math.shapes.VoxelShape;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.fml.network.NetworkHooks;

public class DisenchanttableBlock extends Block {
    protected static final VoxelShape DISENCHANTMENTTABLE_SHAPE = Block.makeCuboidShape(0.0D, 0.0D, 0.0D, 16.0D, 12.0D, 16.0D);
    public static BooleanProperty AUTOMATIC = DisenchanttableProperties.AUTOMATIC;
    public static BooleanProperty BULKDISENCHANTMENT= DisenchanttableProperties.BULKDISENCHANTMENT;
    public static BooleanProperty VOIDING = DisenchanttableProperties.VOIDING;

    public DisenchanttableBlock(Block.Properties properties) {
        super(properties);
        this.setDefaultState(this.stateContainer.getBaseState().with(AUTOMATIC, Boolean.FALSE).with(BULKDISENCHANTMENT, Boolean.FALSE).with(VOIDING, Boolean.FALSE));
        this.setRegistryName(new ResourceLocation(DefaultNames.BLOCK_DISENCHANTTABLE));
    }

    @Override
    protected void fillStateContainer(StateContainer.Builder<Block, BlockState> builder) { builder.add(AUTOMATIC,BULKDISENCHANTMENT, VOIDING); }

    @Override
    @OnlyIn(Dist.CLIENT)
    public boolean hasCustomBreakingProgress(BlockState state)
    {
        return true;
    }

    @Override
    public BlockRenderType getRenderType(BlockState state)
    {
        return BlockRenderType.MODEL;
    }

    @Override
    public VoxelShape getShape(BlockState state, IBlockReader iBlockReader, BlockPos pos, ISelectionContext selectionContext) {
        return DISENCHANTMENTTABLE_SHAPE;
    }
    @Override
    public  boolean hasTileEntity(BlockState state) {
        return true;
    }
    @Override
    public TileEntity createTileEntity(BlockState state, IBlockReader world) {
        return new DisenchanttableTileEntity();
    }

    @Override
    public boolean onBlockActivated(BlockState state, World worldIn, BlockPos pos, PlayerEntity player, Hand handIn, BlockRayTraceResult hit) {
        if (!worldIn.isRemote) {
            TileEntity tileEntity = worldIn.getTileEntity(pos);
            if (tileEntity instanceof INamedContainerProvider) {
                NetworkHooks.openGui((ServerPlayerEntity) player, (INamedContainerProvider) tileEntity, tileEntity.getPos());
            }
        }
        return super.onBlockActivated(state, worldIn, pos, player, handIn, hit);
    }
}
