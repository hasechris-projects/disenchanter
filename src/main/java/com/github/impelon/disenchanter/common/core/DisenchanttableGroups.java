package com.github.impelon.disenchanter.common.core;

import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemStack;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;

public class DisenchanttableGroups {
    public static final ItemGroup DISENCHANTTABLE = new ItemGroup(DisenchanttableReference.MODID) {
        @Override
        @OnlyIn(Dist.CLIENT)
        public ItemStack createIcon() { return new ItemStack(DisenchanttableCore.DisenchanttableBlock); }
    };
}
