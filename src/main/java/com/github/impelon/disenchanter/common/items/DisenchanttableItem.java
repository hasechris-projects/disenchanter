package com.github.impelon.disenchanter.common.items;

import net.minecraft.block.Block;
import net.minecraft.item.BlockItem;
import net.minecraft.item.Item;

public class DisenchanttableItem extends BlockItem {

    public DisenchanttableItem(Block block, Item.Properties item ) {
        super(block, item);

        this.setRegistryName(block.getRegistryName());
    }
}
