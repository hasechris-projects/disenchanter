package com.github.impelon.disenchanter.common.tileentity;

import com.github.impelon.disenchanter.common.blocks.DisenchanttableBlock;
import com.github.impelon.disenchanter.common.core.DisenchanttableCore;
import com.github.impelon.disenchanter.common.core.DisenchanttableReference;
import com.github.impelon.disenchanter.common.util.DefaultNames;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityType;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.registries.IForgeRegistry;
import net.minecraftforge.registries.ObjectHolder;

import java.util.function.Supplier;

public class DisenchanttableTileEntityType {
    @ObjectHolder(DefaultNames.TileEntity_DISENCHANTTABLE)
    public static TileEntityType<?> DISENCHANTTABLE_TileEntity;

    public DisenchanttableTileEntityType()
    {

    }

    @Mod.EventBusSubscriber(modid = DisenchanttableReference.MODID, bus = Mod.EventBusSubscriber.Bus.MOD)
    public static class Registration
    {
        @SubscribeEvent
        public static void onTileEntityRegistry(final RegistryEvent.Register<TileEntityType<?>> e)
        {
            IForgeRegistry<TileEntityType<?>> TileEntityRegistry = e.getRegistry();

            TileEntityRegistry.register(TileEntityType.Builder.create((Supplier<TileEntity>) DisenchanttableTileEntity::new, DisenchanttableCore.DisenchanttableBlock).build(null).setRegistryName(DefaultNames.TileEntity_DISENCHANTTABLE));
        }
    }
}
